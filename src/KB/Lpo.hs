module KB.Lpo
    ( lpo_gt
    , lpo_ge )
where
import Term

lexists :: Applicative f => (a -> f Bool) -> [a] -> f Bool
lexists p [] = pure False
lexists p (x : xs) = pure (||) <*> p x <*> lexists p xs

lforall :: Applicative f => (a -> f Bool) -> [a] -> f Bool
lforall p [] = pure True
lforall p (x : xs) = pure (&&) <*> p x <*> lforall p xs

lite :: Applicative f => f (Bool -> a -> a -> a)
lite = pure ite
    where
      ite True x _ = x
      ite False _ y = y

lexord :: (Eq s,Applicative f) => (s -> s -> f Bool) -> [s] -> [s] -> f Bool
lexord _ [] [] = pure True
lexord ord (x : xs) (y : ys) =
    lite <*> (ord x y)
         <*> (pure (length xs == length ys))
         <*> (pure (x == y) `land` lexord ord xs ys)
lexord _ _ _ = pure False

lor :: Applicative f => f Bool -> f Bool -> f Bool
lor l r = pure (||) <*> l <*> r

land :: Applicative f => f Bool -> f Bool -> f Bool
land l r = pure (&&) <*> l <*> r

lpo_gt :: (Eq s, Applicative f) => (s -> s -> f Bool) -> Term s -> Term s -> f Bool
lpo_gt _ s t | s == t = pure False
lpo_gt ord s t@(Var x) = pure $ occursV x s
lpo_gt ord s@(App f xs) t@(App g ys) =
    (lexists (\ si -> lpo_ge ord si t) xs) `lor`
    (lforall (lpo_gt ord s) ys `land`
     ((pure (f == g) `land` lexord (lpo_gt ord) xs ys) `lor`
      ord f g))
lpo_gt _ _ _ = pure False

lpo_ge :: (Eq s,Applicative f) => (s -> s -> f Bool) -> Term s -> Term s -> f Bool
lpo_ge ord s t
    | s == t = pure True
    | otherwise = lpo_gt ord s t
