{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module KB.UnknownOrd
     ( unknown_ord
     , Unknown
     , runUnknown , debugUnknown
     , unique )
where
import GHC.Base
import Control.Monad
import Control.Monad.State
import qualified Data.List as List
import Debug.Trace

-- FGL
import qualified Data.Graph.Inductive as G
import Data.Graph.Inductive.PatriciaTree (Gr)


data Unknown s a =
  U { unU :: OrdConstraints s -> [(a,OrdConstraints s)] }

interleaveList :: [a] -> [a] -> [a]
interleaveList [] ys = ys
interleaveList (x:xs) ys = x : interleaveList ys xs

interleaveLists :: [[a]] -> [a]
interleaveLists [] = []
interleaveLists (x:xs) = interleaveList x $ interleaveLists xs

unique :: (Eq s, Eq a) => Unknown s a -> Unknown s a
unique (U c) = U $ \ s -> List.nub (c s)

instance Functor (Unknown s) where
    fmap f (U x) = U $ \ s -> fmap (\ (a,b) -> (f a, b)) $ x s

instance Applicative (Unknown s) where
    pure x = U $ \ s -> [(x,s)]
    (<*>) = ap

instance Monad (Unknown s) where
    return = pure
    (U c) >>= k = U $ \ s ->
      interleaveLists $ fmap (\(x,s) -> unU (k x) s) (c s)

instance Alternative (Unknown s) where
    empty   = U $ \ _ -> []
    (U x) <|> (U y) = U $ \ s ->
                interleaveList (x s) (y s)

instance MonadPlus (Unknown s) where
    mplus = (<|>)


runUnknown :: Unknown s a -> [a]
runUnknown (U k) = fmap fst $ k G.empty

debugUnknown :: Unknown s a -> [(a,OrdConstraints s)]
debugUnknown (U k) = k G.empty


type OrdConstraints s = Gr s ()

accessible_from :: Gr s () -> G.Node -> G.Node -> Bool
accessible_from gr to from
    | from == to = True
    | otherwise  =
        maybe False (\_ -> True)
            $ List.find (accessible_from gr to) $ G.suc gr from


nodeForLabel :: Eq s => s -> Gr s () -> Maybe G.Node
nodeForLabel x gr = x `lookup` (fmap (\(x,y) -> (y,x)) $ G.labNodes gr)

freshNode :: Gr s () -> G.Node
freshNode gr = 1 + (foldl max 0 $ G.nodes gr)

get_constraint :: Eq s => OrdConstraints s -> s -> s -> Maybe Ordering
get_constraint gr from to =
  do from <- nodeForLabel from gr
     to   <- nodeForLabel to gr
     if accessible_from gr to from
       then return LT
       else if accessible_from gr from to
            then return GT
            else mzero

get_or_fresh_node :: Eq s => s -> State (Gr s ()) G.Node
get_or_fresh_node s = do gr <- get
                         case nodeForLabel s gr of
                           Nothing -> do let nm = freshNode gr
                                         modify $ G.insNode (nm, s)
                                         return nm
                           Just x -> return x

insert :: Eq s => s -> s -> OrdConstraints s -> OrdConstraints s
insert from to =
    execState $ do from <- get_or_fresh_node from
                   to   <- get_or_fresh_node to
                   modify $ G.insEdges [(from,to,())]

instance MonadState (OrdConstraints s) (Unknown s) where
    get = U $ \ s -> [(s,s)]
    put s = U $ \ _ -> [((),s)]

unknown_ord :: (Eq s,Show s) => s -> s -> Unknown s Bool
unknown_ord l r
    | l == r = return False
    | otherwise =
        do ord <- get
           case get_constraint ord l r of
             Nothing -> trace ("split(" ++ show l ++ "," ++ show r ++ ")") $
               (modify (insert l r) >> return True)
               `mplus`
               (modify (insert r l) >> return False)
             Just GT -> return True
             Just _  -> return False
