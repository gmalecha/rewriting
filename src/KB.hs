{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes #-}
-- | This module implements the Knuth-Bendix completion algorithm
module KB {-
    ( crit_pairs
    , kb_complete
    , normalize
    , rewrite_any
    ) -}
where
import GHC.Base
import Control.Monad
import Control.Monad.State
import Control.DeepSeq
import qualified Data.List as List
import qualified Data.Map as Map
import Text.PrettyPrint.HughesPJClass

import Term
import KB.Lpo

import Debug.Trace

-- | [overlaps y x] returns all substitutions such that
-- [y] unifies with a subterm of [x] that is not a variable
overlaps :: Eq s
           => Term s -- Term to unify with
           -> Term s -- Term to replace with
           -> Term s -- Term to search in
           -> [(Inst s, Term s)]
overlaps y rep x@(App f xs) = here `mplus` elsewhere [] xs
    where
      here =
          case unify x y inst_empty of
            Nothing -> mzero
            Just s  -> return (s,rep)
      elsewhere _ [] = mzero
      elsewhere zs (x : xs) =
          (fmap (\ (s,t) -> (s, App f $ zs ++ t : xs)) $ overlaps y rep x)
          `mplus` elsewhere (zs ++ [x]) xs
overlaps _ _ (UVar _) = mzero
overlaps _ _ (Var _) = mzero


crit_pairs_with :: Eq s => Rw s -> [Rw s] -> [Rw s]
crit_pairs_with rw' rws =
    do rw' <- fmap (openRw $ rwVars rw) rws
       (i,t) <- overlaps (lhs rw') (rhs rw') (lhs rw)
       let l = inst i $ rhs rw
       let r = inst i $ t
       guard $ l /= r
       return $ closeRw $ Rw l r
    where
      rw = openRw Nothing rw'

-- | Compute the critical pairs of a list of equations
crit_pairs :: (Eq s,Pretty s,NFData s) => [Rw s] -> [Rw s]
crit_pairs rws = filter_crit_pairs rws crs
  where
    crs = do rw <- rws
             crit_pairs_with rw rws

filter_crit_pairs :: (Eq s,Pretty s,NFData s) => [Rw s] -> [Rw s] -> [Rw s]
filter_crit_pairs eqs crit =
  do (Rw l r) <- crit
     let l' = normalize eqs l
     let r' = normalize eqs r
     guard $ l' /= r'
     return $ Rw l' r'

-- | The rewrites are oriented left-to-right
rewrite :: Eq s => Rw s -> Term s -> Maybe (Term s)
rewrite rw t' = rewrite_unsafe rw' t
    where
      t = normalizeTerm t'
      rw' = liftRw (maxVars t) $ normalizeRw rw

rewrite_unsafe :: Eq s => Rw s -> Term s -> Maybe (Term s)
rewrite_unsafe rw t =
  fmap (\(i,t) -> inst i t) $ fmap fst $ List.uncons $ overlaps l r t
  where
    (Rw l r) = openRw (maxVars t) rw

rewrite_any :: Eq s => [Rw s] -> Term s -> Maybe (Term s)
rewrite_any [] t = Nothing
rewrite_any (rw : rws) t = (rewrite rw t) `mplus` rewrite_any rws t

fixpoint :: (a -> Maybe a) -> a -> a
fixpoint f x = maybe x (fixpoint f) $ f x

normalize :: (Eq s,Pretty s,NFData s) => [Rw s] -> Term s -> Term s
normalize eqs t =
  x `deepseq` x
  where
    x = fixpoint (\ x -> {- trace ("f.." ++ (render $ pPrint $ x)) $ -} rewrite_any eqs x) t

-- | This removes redundant rewrites
reduce_rewrites :: (Eq s,Pretty s,NFData s) => [Rw s] -> [Rw s]
reduce_rewrites = rr []
  where
    rr acc [] = acc
    rr acc (Rw l r : xs)
      | normalize (acc ++ xs) l == normalize (acc ++ xs) r = rr acc xs
      | otherwise =
          rr (Rw (normalize (acc ++ xs) l) (normalize (acc ++ xs) r) : acc) xs

-- | Knuth-Bendix Completion
kb_complete :: (Pretty s, Eq s, NFData s, Monad m, MonadPlus m)
            => (s -> s -> m Bool) {- This is the comparison function -}
            -> (forall a. Eq a => m a -> m a)
            -> [Rw s]
            -> m [Rw s]
kb_complete ord unique rws =
  fmap reduce_rewrites
     $ evalStateT (complete ord unique False [] $ rws ++ crit_pairs rws) []

complete :: (Pretty s, Eq s, NFData s, Monad m, MonadPlus m)
         => (s -> s -> m Bool) {- ordering -}
         -> (forall s. Eq s => m s -> m s)
         -> Bool     {- Progress  -}
         -> [Rw s]   {- Equations -}
         -> [Rw s]   {- Critical pairs -}
         -> StateT [Rw s] m [Rw s] {- Rewrites -}
{-
complete _ _ _ eqs crs
  | trace ("complete (" ++ show (length eqs) ++ "," ++ show (length crs) ++ ")") False = undefined
-}
complete ord unique prog eqs [] =
    do def <- get -- Take and remove defers set
       put []
       trace "processing defer" $ continue def
    where
      continue [] = return eqs
      continue def
        | prog      = complete ord unique False eqs def
        | otherwise = lift mzero
complete ord unique prog eqs (Rw l r : crs) =
    if l' == r' then
        complete ord unique prog eqs crs
    else
        do orw <- lift $ unique $ orient ord l' r'
           case orw of
             Nothing -> do trace "defer" $ modify ((:) (Rw l r))
                           complete ord unique prog eqs crs
             Just EQ -> undefined -- complete prog eqs crs
             Just x -> do let rw' = closeRw $ toRw x
                          let eqs' = rw' : eqs
                          let ncrs = crit_pairs_with rw' eqs' {-
                          trace ("adding rewrite " ++ render (pPrint rw'))
                            $ trace ("new_crit_pairs = " ++ (show $ length ncrs))
                              $ -}
                          complete ord unique True eqs' $ crs ++ ncrs
    where
      l' = normalize eqs l
      r' = normalize eqs r

      toRw LT = Rw l' r'
      toRw GT = Rw r' l'
      toRw _  = undefined -- imposssible


orient :: (Monad m, Eq s, MonadPlus m)
       => (s -> s -> m Bool)
       -> Term s
       -> Term s
       -> m (Maybe Ordering)
orient ord l r =
    (do lpo_gt ord l r >>= guard
        return $ Just LT)
    `mplus`
    (do lpo_gt ord r l >>= guard
        return $ Just GT)
