{-# LANGUAGE DeriveGeneric, DeriveAnyClass #-}
-- | This is the term language & the unification environment
module Term where
import qualified Data.Map.Strict as Map
import Control.Monad
import Control.Monad.State
import Control.Exception.Base (assert)
import Control.DeepSeq
import GHC.Generics (Generic)
import Text.PrettyPrint
import Text.PrettyPrint.HughesPJClass

-- | The representation of unification variables
newtype Unif = Unif Integer
    deriving (Eq,Show,Ord,Generic,NFData)
unUnif (Unif x) = x

mkU :: Integer -> Term s
mkU u = UVar $ Unif u

mkV :: Integer -> Term s
mkV = Var

instance Pretty Unif where
    pPrint (Unif u) = integer u

-- | The term language
data Term s =
      App s [Term s]
    | UVar Unif
    | Var Integer
    deriving (Eq, Show, NFData, Generic)

instance Pretty s => Pretty (Term s) where
    pPrint (UVar u) = text "?" <> pPrint u
    pPrint (Var u) = text "#" <> integer u
    pPrint (App f []) = pPrint f
    pPrint (App f xs) = parens $ pPrint f <+> (sep $ fmap pPrint xs)

-- | Rewrites (left-to-right)
-- Note: Rewrites will always be normalized to use variables from 0 upward
data Rw s =
    Rw { lhs :: Term s , rhs :: Term s }
    deriving (Eq, Show)

openTerm :: Maybe Integer -> Term s -> Term s
openTerm i (UVar _) = assert False $ undefined "invariant violation"
openTerm i (Var v) = UVar $ liftUnif i $ Unif v
openTerm i (App f xs) = App f $ fmap (openTerm i) xs

openRw :: Maybe Integer -> Rw s -> Rw s
openRw i (Rw l r) = Rw (openTerm i l) (openTerm i r)

instance Pretty s => Pretty (Rw s) where
    pPrint (Rw l r) = pPrint l <+> text "=" <+> pPrint r

maxVarCount :: Maybe Integer -> Maybe Integer -> Maybe Integer
maxVarCount Nothing x = x
maxVarCount x Nothing = x
maxVarCount (Just a) (Just b) = Just $ max a b

minVarCount :: Maybe Integer -> Maybe Integer -> Maybe Integer
minVarCount Nothing x = x
minVarCount x Nothing = Nothing
minVarCount (Just a) (Just b) = Just $ min a b

maxVars :: Term s -> Maybe Integer
maxVars (UVar u) = Just $ unUnif u
maxVars (Var u) = Nothing
maxVars (App _ xs) = foldl maxVarCount Nothing $ fmap maxVars xs

minVars :: Term s -> Maybe Integer
minVars (UVar u) = Just $ unUnif u
minVars (Var u) = Nothing
minVars (App _ xs) = foldl minVarCount Nothing $ fmap minVars xs

rwVars :: Rw s -> Maybe Integer
rwVars rw = (maxVars $ lhs rw) `maxVarCount` (maxVars $ rhs rw)

liftUnif :: Maybe Integer -> Unif -> Unif
liftUnif Nothing x = x
liftUnif (Just n) (Unif u) = Unif $ 1 + n + u

liftTerm :: Maybe Integer -> Term s -> Term s
liftTerm c (App f xs) = App f $ fmap (liftTerm c) xs
liftTerm c (UVar u) = UVar $ liftUnif c u
liftTerm c (Var u) = Var u

liftRw :: Maybe Integer -> Rw s -> Rw s
liftRw c rw = Rw (liftTerm c $ lhs rw) (liftTerm c $ rhs rw)

flipRw :: Rw s -> Rw s
flipRw (Rw l r) = Rw r l

normalizeRw :: Rw s -> Rw s
normalizeRw rw = liftRw neg_c rw
    where
      c = (minVars $ lhs rw) `minVarCount` (minVars $ rhs rw)
      neg_c = fmap (\ x -> - (x + 1)) c

normalizeTerm :: Term s -> Term s
normalizeTerm t = liftTerm neg_c t
    where
      c = minVars t
      neg_c = fmap (\ x -> - (x + 1)) c

normalInst' :: Term s -> State (Inst s, Integer) ()
normalInst' (Var _) = assert False $ undefined "invariant violation"
normalInst' (UVar x) =
  do (i,f) <- get
     case inst_lookup i x of
       Nothing -> put (Inst (Map.insert x (Var f) $ unInst i), f+1)
       Just _ -> return ()
normalInst' (App f xs) = sequence_ $ fmap normalInst' xs

closeRw :: Rw s -> Rw s
closeRw (Rw l r) = Rw (inst st l) (inst st r)
    where
      (st,_) = execState (normalInst' l >> normalInst' r) (inst_empty, 0)

-- | Instantiations/Substitutions (Unif -> Term s)
newtype Inst s = Inst (Map.Map Unif (Term s))

unInst (Inst s) = s

inst_empty :: Inst s
inst_empty = Inst Map.empty

inst_lookup :: Inst s -> Unif -> Maybe (Term s)
inst_lookup i x = x `Map.lookup` unInst i

inst_set :: Unif -> Term s -> Inst s -> Maybe (Inst s)
inst_set u t i =
    case inst_lookup i u of
      Just _ -> assert False $ Nothing
      Nothing -> if occurs u (inst i t) then mzero
                 else return $ Inst $ Map.insert u t $ unInst i

inst :: Inst s -> Term s -> Term s
inst i (App f xs) = App f $ fmap (inst i) xs
inst i (UVar u) = maybe (UVar u) (inst i) $ inst_lookup i u
inst _ (Var v) = Var v

instRw :: Inst s -> Rw s -> Rw s
instRw i (Rw l r) = Rw (inst i l) (inst i r)

-- | The occurs check
occurs :: Unif -> Term s -> Bool
occurs u' (UVar u) = u == u'
occurs _ (Var _) = False
occurs u' (App _ xs) = foldl (||) False $ fmap (occurs u') xs

occursV :: Integer -> Term s -> Bool
occursV _ (UVar u) = False
occursV v' (Var v) = v == v'
occursV u' (App _ xs) = foldl (||) False $ fmap (occursV u') xs

-- | Unification of two terms
unify :: (Eq s) => Term s -> Term s -> Inst s -> Maybe (Inst s)
unify (App f xs) (App g ys) i | f == g && length xs == length ys =
                                  foldM (\i (l,r) -> unify l r i) i $ zip xs ys
                              | otherwise = mzero
unify (UVar u) x i =
    case inst_lookup i u of
      Just v -> unify (inst i v) x i
      Nothing -> if x' == UVar u then return i
                 else if occurs u x' then mzero
                      else inst_set u x' i
    where
      x' = inst i x
unify x (UVar u) i = unify (UVar u) x i
unify (Var a) (Var b) i
    | a == b = Just i
    | otherwise = Nothing
unify _ _ _ = Nothing
