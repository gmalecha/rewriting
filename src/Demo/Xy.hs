module Demo.Xy where
import Term

xy_rws :: [Rw String]
xy_rws = [ Rw (s "x" `x` s "x" `x` s "x") (s "1")
           , Rw (s "y" `x` s "y" `x` s "y") (s "1")
           , Rw (s "x" `x` s "y" `x` s "x" `x` s "y" `x` s "x" `x` s "y") (s "1")
           , Rw (s "1" `x` mkV 0) (mkV 0)
           , Rw ((mkV 0 `x` mkV 1) `x` mkV 2)
                (mkV 0 `x` (mkV 1 `x` mkV 2)) ]
    where
      s x = App x []
      x a b = App "*" [a,b]

ffg_rws :: [Rw String]
ffg_rws =
    [ Rw (App "f" [App "f" [mkV 0]])
         (App "g" [mkV 0]) ]
