module Demo.Group where
import Term

group_rws :: [Rw String]
group_rws =
    [ Rw (App "*" [App "*" [mkV 0, mkV 1], mkV 2])
         (App "*" [mkV 0, App "*" [mkV 1, mkV 2]])
    , Rw (App "*" [App "1" [], mkV 0])
         (mkV 0)
    , Rw (App "*" [App "i" [mkV 0], mkV 0])
         (App "1" []) ]

paper_rws :: [Rw String]
paper_rws = [ Rw (s "0" `x` mkV 0) (mkV 0)
            , Rw (mkV 0 `x` s "0") (mkV 0)
            , Rw (App "-" [mkV 0] `x` mkV 0) (s "0")
            , Rw ((mkV 0 `x` mkV 1) `x` mkV 2)
                 (mkV 0 `x` (mkV 1 `x` mkV 2)) ]
    where
      s x = App x []
      x a b = App "+" [a,b]
