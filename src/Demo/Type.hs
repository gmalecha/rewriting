{-# LANGUAGE DeriveGeneric, DeriveAnyClass #-}
module Demo.Type
where
import Prelude (Char,Eq,Show,($),(.),show,(++))
import Control.DeepSeq
import GHC.Generics (Generic)
import Term
import Text.PrettyPrint.HughesPJClass

data Sym = One
         | Zero
         | Neg
         | Plus | Mult
         | Leq
         | T
         | Not
         | And | Or
         | Eq
         | Dot
         | Chr Char
         | Emp
           deriving (Eq, Show, NFData, Generic)

instance Pretty Sym where
    pPrint T = text "⊤"
    pPrint Plus = text "+"
    pPrint Eq = text "="
    pPrint x = text $ show x

bool_alg :: [Rw Sym]
bool_alg = [ Rw (not $ not $ mkV 0) (mkV 0)
           , Rw (mkV 0 `and` true) (mkV 0)
           , Rw (mkV 0 `and` not (mkV 0)) false
           , comm And
           , Rw (mkV 0 `or` mkV 1) (not $ not (mkV 0) `and` not (mkV 1))
           , Rw (mkV 0 `and` (mkV 1 `or` mkV 2))
                ((mkV 0 `and` mkV 1) `or` (mkV 0 `and` mkV 2)) ]

comm_ring :: [Rw Sym]
comm_ring = [ assoc Plus
            , runit Plus zero
            , comm Plus
            , Rw (mkV 0 `plus` neg (mkV 0)) zero
            , assoc Mult
            , runit Mult one
            , comm Mult
            , Rw (mkV 0 `mult` (mkV 1 `plus` mkV 2))
                 ((mkV 0 `mult` mkV 1) `plus` (mkV 0 `mult` mkV 2))
            ]

tor :: [Rw Sym]
tor = [ Rw ((mkV 0 `leq` mkV 1) `or` (mkV 1 `leq` mkV 0)) true -- Typo
      , Rw ((not $ (mkV 0 `leq` mkV 1) `and` (mkV 2 `leq` mkV 3))
            `or` ((mkV 0 `plus` mkV 2) `leq` (mkV 1 `plus` mkV 3))) true
      , Rw ((not $ (mkV 0 `leq` mkV 1) `and` (zero `leq` mkV 2))
            `or` ((mkV 0 `mult` mkV 2) `leq` (mkV 1 `mult` mkV 2))) true
      , Rw (one `leq` zero) false
      ]

monoid :: [Rw Sym]
monoid = [ Rw (mkV 0 `dot` emp) (mkV 0)
         , Rw (emp `dot` mkV 0) (mkV 0)
         , assoc Dot ]

congruence :: [Rw Sym]
congruence = [ Rw (mkV 0 `eq` mkV 0) true
             , Rw ((mkV 0 `eq` mkV 1) `impl` (mkV 1 `eq` mkV 0)) true
             , Rw (((mkV 0 `eq` mkV 1) `and` (mkV 1 `eq` mkV 2))
                   `impl` (mkV 0 `eq` mkV 2)) true
             , Rw (((mkV 0 `eq` mkV 1) `and` (mkV 2 `eq` mkV 3))
                   `impl` ((mkV 0 `dot` mkV 2) `eq` (mkV 1 `dot` mkV 3))) true
             ]

all :: [Rw Sym]
all = bool_alg ++ comm_ring ++ tor ++ monoid ++ congruence

comm , assoc :: Sym -> Rw Sym
comm x = Rw (mkV 0 `bx` mkV 1) (mkV 1 `bx` mkV 0)
    where bx = bin x
assoc x = Rw ((mkV 0 `bx` mkV 1) `bx` mkV 2)
             (mkV 1 `bx` (mkV 0 `bx` mkV 2))
    where bx = bin x

runit :: Sym -> Term Sym -> Rw Sym
runit p u = Rw (mkV 0 `bp` u) (mkV 0)
    where bp = bin p

bin c a b = App c [a,b]

plus, mult, and, or, dot, eq , impl:: Term Sym -> Term Sym -> Term Sym
plus = bin Plus
mult = bin Mult
and = bin And
or = bin Or
dot = bin Dot
eq = bin Eq
leq = bin Leq
impl a b = not a `or` b

neg , not :: Term Sym -> Term Sym
neg x = App Neg [x]
not x = App Not [x]

one , zero , true , false , emp :: Term Sym
one = App One []
zero = App Zero []
true = App T []
false = not true
emp = App Emp []
