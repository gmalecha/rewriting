module Main
   ( main )
where
import Text.PrettyPrint.HughesPJClass
import Control.Monad
import Term
import KB
import KB.UnknownOrd
import KB.Lpo
import Control.DeepSeq
import Demo.Group
import Demo.Xy

test :: (Eq s,Show s,NFData s,Pretty s) => String -> [Rw s] -> IO ()
test name rws = do
  let result = runUnknown $ kb_complete unknown_ord unique rws
  putStrLn $ name
  putStrLn $ render $ pPrint $ fmap reduce_rewrites result
  where
    only_one [] = Nothing
    only_one (x:_) = Just x

main :: IO ()
main = do
  test "paper_rws" paper_rws
  test "group_rws" group_rws
  test "ffg_rws" ffg_rws
  test "xy_rws" xy_rws
